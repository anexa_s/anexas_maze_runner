from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='game',
    version='0.0.1',
    description='AI project',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent'
    ],
    url='https://github.com/ktk53x/game',
    author='ktk and klexis',
    author_email='kartikeya.0005@gmail.com',
    keywords='AI',
    packages=['game'],
    install_requires=[],
    include_package_data=True,
)
