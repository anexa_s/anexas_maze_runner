from abc import ABC, abstractmethod
from main_game import Game
from game_state import GameState


class Agent(ABC):

    def __init__(self):
        self.__dx = 0
        self.__dy = 0
        game = Game.get_instance()
        self._canvas = game.get_canvas()
        self._game_window = game.get_game_window()
        self._unit = game.get_unit()
        self._shape = None
        self._timer = 200
        self._game_state = GameState.get_instance()

    def set_shape(self, shape):
        self._shape = shape

    @abstractmethod
    def think(self):
        pass

    def move(self):
        shape = self._shape.get_shape()
        if shape:
            self.think()

            position = self._shape.get_position()
            (x, y) = position
            x += self.__dx
            y += self.__dy
            next_position = (x, y)
            if not self._game_state.is_wall(next_position):
                self._game_state.move_character(position, next_position)
                self._shape.set_position(next_position)
                self._canvas.move(shape, self.__dx * self._unit, self.__dy * self._unit)
            self._canvas.after(self._timer, self.move)

    def _go_left(self, event=None):
        self.__dx = -1
        self.__dy = 0

    def _go_right(self, event=None):
        self.__dx = 1
        self.__dy = 0

    def _go_up(self, event=None):
        self.__dx = 0
        self.__dy = -1

    def _go_down(self, event=None):
        self.__dx = 0
        self.__dy = 1


class KeyBoardAgent(Agent):

    def __init__(self):
        super().__init__()

    def think(self):
        self._game_window.bind("<KeyPress-Left>", self._go_left)
        self._game_window.bind("<KeyPress-Right>", self._go_right)
        self._game_window.bind("<KeyPress-Up>", self._go_up)
        self._game_window.bind("<KeyPress-Down>", self._go_down)
