from heapq import *


def unmap(position, unit):
    (x, y) = position
    x = (x - unit) // unit
    y = (y - unit) // unit
    return x, y


def remap(position, unit):
    (x, y) = position
    x = unit * x + unit
    y = unit * y + unit
    return x, y


class Stack:

    def __init__(self):
        self.stack = []

    def push(self, element):
        self.stack.append(element)

    def pop(self):
        return self.stack.pop()

    def is_empty(self):
        return len(self.stack) == 0


class Queue:

    def __init__(self):
        self.queue = []

    def enqueue(self, element):
        self.queue.insert(0, element)

    def dequeue(self):
        return self.queue.pop()

    def is_empty(self):
        return len(self.queue) == 0


class PriorityQueue:

    def __init__(self):
        self.heap = []
        self.count = 0

    def push(self, element, priority):
        entry = (priority, self.count, element)
        heappush(self.heap, entry)
        self.count += 1

    def pop(self):
        (_, _, element) = heappop(self.heap)
        return element

    def is_empty(self):
        return len(self.heap) == 0
