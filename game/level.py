from abc import ABC, abstractmethod
from character import Character
from agent import KeyBoardAgent
from main_game import Game
from game_state import GameState
from shape import Square, Circle


class Level(ABC):
    def __init__(self):
        self._game = Game.get_instance()
        self._game_state = GameState.get_instance()

    def create_level(self):
        self.create_walls()
        self.create_pellets()
        self.create_characters()

    @abstractmethod
    def create_characters(self):
        pass

    @abstractmethod
    def create_pellets(self):
        pass

    @abstractmethod
    def create_walls(self):
        pass


class Level0(Level):

    def __init__(self):
        super().__init__()

    def create_characters(self):
        """Single Character Level"""
        characters = {}
        unit = self._game.get_unit()
        position = (1, 1)
        shape = Circle(position, unit // 2)
        agent = KeyBoardAgent()
        bot = Character(shape)
        bot.set_agent(agent)

        characters[position] = bot
        self._game_state.set_characters(characters)

        bot.start_moving()

    def create_pellets(self):
        """Single pellet"""
        height = self._game.get_height()
        width = self._game.get_width()
        unit = self._game.get_unit()
        pellets = {}

        position = (10, 10)
        shape = Circle(position, unit // 4)
        pellets[position] = shape

        self._game_state.set_pellets(pellets)

    def create_walls(self):
        """Walls only at the borders"""
        height = self._game.get_height()
        width = self._game.get_width()
        unit = self._game.get_unit()
        walls = {}

        for i in range(height):
            position = (i, 0)
            wall = Square(position, unit)
            walls[position] = wall

            position = (i, width - 1)
            wall = Square(position, unit)
            walls[position] = wall

        for j in range(width):
            position = (0, j)
            wall = Square(position, unit)
            walls[position] = wall

            position = (height - 1, j)
            wall = Square(position, unit)
            walls[position] = wall

        self._game_state.set_walls(walls)
