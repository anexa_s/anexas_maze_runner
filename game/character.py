import uuid


class Character:

    def __init__(self, shape):
        self.__shape = shape
        self.__id = uuid.uuid1()
        self.__agent = None
        print("Creating character {}, {}...".format(self.__id, self.__shape))

    def set_agent(self, agent):
        self.__agent = agent
        print("Setting agent {} to character {}...".format(agent, self.__id))
        self.__agent.set_shape(self.__shape)

    def start_moving(self):
        if self.__agent:
            self.__agent.move()
