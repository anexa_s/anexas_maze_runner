from main_game import Game
from shape import Square, Circle
from character import Character
from agent import KeyBoardAgent
from level import Level0
from game_state import GameState

game = Game.get_instance()

# for i in range(game.get_height()):
#     for j in range(game.get_width()):
#         shape = Square((i, j), game.get_unit())

# shape = Circle((0, 0), game.get_unit() // 2)
# c = Character(shape)
# c.set_agent(KeyBoardAgent())
# c.start_moving()

level = Level0()
level.create_level()
game_state = GameState.get_instance()
# game_state.print()
game.run()
