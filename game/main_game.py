from tkinter import Tk, Canvas


class Game:

    game = None

    def create_game_window(self):
        if not self.__game_window:
            print("Creating a new game window...")
            self.__game_window = Tk()
        return self.__game_window

    def create_main_canvas(self, color="SpringGreen2"):
        if not self.__canvas and self.__game_window:
            print("Creating a new canvas...")
            self.__canvas = Canvas(self.__game_window, bg=color, height=self.__canvas_y, width=self.__canvas_x)
        return self.__canvas

    def run(self):
        if self.__canvas and self.__game_window:
            print("Running game...")
            self.__canvas.pack()
            self.__game_window.mainloop()

    def destroy_game_window(self):
        if self.__game_window:
            print("Game over!!!")
            self.__game_window.destroy()
        return self.__game_window

    def get_canvas(self):
        return self.__canvas

    def get_game_window(self):
        return self.__game_window

    def get_unit(self):
        return self.__unit

    def get_height(self):
        return self.__height

    def get_width(self):
        return self.__width

    @staticmethod
    def get_instance():
        if not Game.game:
            print("Creating a new game instance...")
            Game.game = Game()
        return Game.game

    def __init__(self):
        self.__game_window = None
        self.__canvas_x = 500
        self.__canvas_y = 500
        self.__unit = 20
        self.__height = (self.__canvas_x - self.__unit) // self.__unit
        self.__width = (self.__canvas_y - self.__unit) // self.__unit
        self.__canvas = None

        self.__game_window = self.create_game_window()
        self.__canvas = self.create_main_canvas()
