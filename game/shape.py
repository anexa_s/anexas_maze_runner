from abc import ABC, abstractmethod
from main_game import Game
from utils import remap, unmap


class Shape(ABC):

    def __init__(self, position, color):
        game = Game.get_instance()
        self._canvas = game.get_canvas()
        self._unit = game.get_unit()

        self._position = remap(position, self._unit)
        self._color = color
        self._shape = None

    def get_shape(self):
        return self._shape

    def get_position(self):
        return unmap(self._position, self._unit)

    def set_position(self, position):
        self._position = remap(position, self._unit)

    @abstractmethod
    def draw(self):
        pass

    def erase(self):
        if self._shape:
            print("Erasing shape {}...".format(self))
            self._canvas.delete(self._shape)
        else:
            print("Shape has already been deleted...")


class Square(Shape):

    def __init__(self, position, length, color="SpringGreen4", outline="dark green"):
        super().__init__(position, color)
        self.__length = length
        self.__outline = outline
        print("Creating square {}...".format(self))
        self.draw()

    def draw(self):
        (x, y) = self._position
        radius = self.__length / 2
        color = self._color
        outline = self.__outline
        self._shape = self._canvas.create_rectangle(x - radius, y + radius, x + radius, y - radius,
                                                    outline=outline, fill=color)


class Circle(Shape):

    def __init__(self, position, radius, color="RoyalBlue1", outline="RoyalBlue4"):
        super().__init__(position, color)
        self.__radius = radius
        self.__outline = outline
        print("Creating circle {}...".format(self))
        self.draw()

    def draw(self):
        (x, y) = self._position
        radius = self.__radius
        outline = self.__outline
        color = self._color
        self._shape = self._canvas.create_oval(x - radius, y + radius, x + radius, y - radius,
                                               outline=outline, fill=color)
