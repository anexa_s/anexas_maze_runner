
class GameState:
    game_state = None

    @staticmethod
    def get_instance():
        if not GameState.game_state:
            GameState.game_state = GameState()
        return GameState.game_state

    def __init__(self):
        self.__walls = dict()
        self.__characters = dict()
        self.__pellets = dict()

    def print(self):
        print("walls: ", self.__walls)
        print("characters: ", self.__characters)
        print("pellets: ", self.__pellets)

    def set_walls(self, walls):
        self.__walls = walls

    def set_characters(self, characters):
        self.__characters = characters

    def set_pellets(self, pellets):
        self.__pellets = pellets

    def delete_pellet(self, position):
        self.__pellets.pop(position)

    def is_wall(self, position):
        return position in self.__walls

    def move_character(self, position, new_position):
        character = self.__characters.get(position)
        self.__characters.pop(position)
        self.__characters[new_position] = character

