# Game
## About the project

## How to install
### Open a new terminal
1. Create a new enlistment for the project:
```console
foo@bar:~$ git clone "https://kartikeya0005@dev.azure.com/kartikeya0005/game/_git/game"
foo@bar:~$ cd game
```
2. Create a new virtual environment and activate it:
```console
foo@bar:~$ python -m venv env
foo@bar:~$ source env/bin/activate
``` 
3. Upgrade pip installer (Note: make sure you have python installed):
```console
foo@bar:~$ python -m pip install --upgrade pip
```
4. Install the project
```console
foo@bar:~$ pip install -e .
foo@bar:~$ python setup.py install
```